package pageObjects;

import org.openqa.selenium.By;

public class pageObjects {

    public static final By CLOSE_COOKIE_POPUP = By.className("didomi-dismiss-button");
    public static final By CLOSE_LIGHTBOX = By.xpath("//button[contains(text(),'X')]");
    public static final By MENU_LOGIN = By.cssSelector("*[data-qa='login']");
    public static final By EMAIL_ADDRESS_FIELD = By.name("email");
    public static final By PASSWORD_FIELD = By.name("password");
    public static final By LOGIN_BUTTON = By.cssSelector("*[data-qa='login']");
    public static final By USER_GREETING = By.className("header__link--user");
}
