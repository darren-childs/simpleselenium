package Interface;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class interactions {

    private static WebDriver driver;
    private static WebDriverWait wait;
    private static WebElement focusedElement;

    public static void setup(){
        System.setProperty("webdriver.chrome.driver", "/Users/localadmin/Documents/drivers/chromedriver");
        //create new chromedriver instance
        driver = new ChromeDriver();
        //Add a wait, time to wait for elements before test will time out.
        wait = new WebDriverWait(driver, 30);
    }

    public static void navigateTo(String string){
        driver.get(string);
    }

    public static void focus(By by){
        focusedElement = wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public static void click(){
        focusedElement.click();
    }

    public static void compose(String string){
        focusedElement.sendKeys(string);
    }

    public static void isDisplayed(){
        focusedElement.isDisplayed();
    }

    public static void quit(){
        driver.quit();
    }
}
