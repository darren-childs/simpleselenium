package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This code demonstrates a basic WebDriver test
 *
 */
public class BasicTest1 {

    private static WebDriver driver;
    private static WebDriverWait wait;



    public static void main( String[] args ) {

        //Tell the code where to find chromedriver on the system
        System.setProperty("webdriver.chrome.driver", "/Users/localadmin/Documents/drivers/chromedriver");
        //create new chromedriver instance
        driver = new ChromeDriver();

        //Add a wait, time to wait for elements before test will time out.
        wait = new WebDriverWait(driver, 30);

        //Navigate to a url
        driver.get("http://www.wowcher.co.uk");

        //Dismiss cookie popup
        wait.until(ExpectedConditions.elementToBeClickable(By.className("didomi-dismiss-button"))).click();
        //Close the lightbox
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'X')]"))).click();

        //Navigate to Login
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("*[data-qa='login']"))).click();
        //Fill out login form and submit
        wait.until(ExpectedConditions.elementToBeClickable(By.name("email"))).sendKeys("darren.childs@wowcher.co.uk");
        wait.until(ExpectedConditions.elementToBeClickable(By.name("password"))).sendKeys("Password1");
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("*[data-qa='login']"))).click();

        //Verify that user has logged in
        wait.until(ExpectedConditions.elementToBeClickable(By.className("header__link--user"))).isDisplayed();

        driver.quit();

    }
}
