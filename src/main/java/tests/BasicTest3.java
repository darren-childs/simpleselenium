package tests;

import pageObjects.pageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This code demonstrates a more advanced way of writing a test than BasicTest2.java
 * BasicTest3 uses a Page Object class to pass page objects into the methods.
 *
 */

public class BasicTest3 {

    private static WebDriver driver;
    private static WebDriverWait wait;
    private static WebElement focusedElement;

    public static pageObjects pages = new pageObjects();

    public static void main( String[] args ) {

        //Tell the code where to find chromedriver on the system
        System.setProperty("webdriver.chrome.driver", "/Users/localadmin/Documents/drivers/chromedriver");
        //create new chromedriver instance
        driver = new ChromeDriver();

        //Add a wait, time to wait for elements before test will time out.
        wait = new WebDriverWait(driver, 30);

        //Navigate to a url
        navigateTo("http://www.wowcher.co.uk");

        //Pass an element to the focus method
        focus(pages.CLOSE_COOKIE_POPUP);
        //Then click that element
        click();

        focus(pages.CLOSE_LIGHTBOX);
        click();

        //Navigate to Login
        focus(pages.MENU_LOGIN);
        click();
        //Fill out login form and submit
        focus(pages.EMAIL_ADDRESS_FIELD);
        compose("darren.childs@wowcher.co.uk");

        focus(pages.PASSWORD_FIELD);
        compose("Password1");

        focus(pages.LOGIN_BUTTON);
        click();

        //Verify that user has logged in
        focus(pages.USER_GREETING);
        isDisplayed();

        //Quit the driver
        quit();
    }

    public static void navigateTo(String string){
        driver.get(string);
    }

    public static void focus(By by){
        focusedElement = wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public static void click(){
        focusedElement.click();
    }

    public static void compose(String string){
        focusedElement.sendKeys(string);
    }

    public static void isDisplayed(){
        focusedElement.isDisplayed();
    }

    public static void quit(){
        driver.quit();
    }
}
