package tests;

import Interface.interactions;
import pageObjects.pageObjects;

/**
 * This code demonstrates a more advanced way of writing a test than BasicTest3.java
 * BasicTest4 uses the interactions class, all methods have now been moved to the interactions class
 * The only code that exists in this file now is the test code
 */

public class BasicTest4 extends interactions {

    public static pageObjects pages = new pageObjects();

    public static void main( String[] args ) {

        setup();

        //Navigate to a url
        navigateTo("http://www.wowcher.co.uk");

        //Pass an element to the focus method
        focus(pages.CLOSE_COOKIE_POPUP);
        //Then click that element
        click();

        focus(pages.CLOSE_LIGHTBOX);
        click();

        //Navigate to Login
        focus(pages.MENU_LOGIN);
        click();
        //Fill out login form and submit
        focus(pages.EMAIL_ADDRESS_FIELD);
        compose("darren.childs@wowcher.co.uk");

        focus(pages.PASSWORD_FIELD);
        compose("Password1");

        focus(pages.LOGIN_BUTTON);
        click();

        //Verify that user has logged in
        focus(pages.USER_GREETING);
        isDisplayed();

        //Quit the driver
        quit();
    }


}
