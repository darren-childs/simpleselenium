package tests;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This code demonstrates a more advanced way of writing a test than BasicTest1.java
 * BasicTest2 uses methods to pass parameters such as page objects, and perform actions such as click.
 *
 */

public class BasicTest2 {

    private static WebDriver driver;
    private static WebDriverWait wait;
    private static WebElement focusedElement;

    public static void main( String[] args ) {

        //Tell the code where to find chromedriver on the system
        System.setProperty("webdriver.chrome.driver", "/Users/localadmin/Documents/drivers/chromedriver");
        //create new chromedriver instance
        driver = new ChromeDriver();

        //Add a wait, time to wait for elements before test will time out.
        wait = new WebDriverWait(driver, 30);

        //Navigate to a url
        navigateTo("http://www.wowcher.co.uk");

        //Pass an element to the focus method
        focus(By.className("didomi-dismiss-button"));
        //Then click that element
        click();

        focus(By.xpath("//button[contains(text(),'X')]"));
        click();

        //Navigate to Login
        focus(By.cssSelector("*[data-qa='login']"));
        click();
        //Fill out login form and submit
        focus(By.name("email"));
        compose("darren.childs@wowcher.co.uk");

        focus(By.name("password"));
        compose("Password1");

        focus(By.cssSelector("*[data-qa='login']"));
        click();

        //Verify that user has logged in
        focus(By.className("header__link--user"));
        isDisplayed();

        //Quit the driver
        quit();
    }

    //Navigate to a browser URL
    public static void navigateTo(String string){
        driver.get(string);
    }

    //Focus on a element using By selector, waits for it to be clickable. Sets the value of focusedElement to be by parameter
    public static void focus(By by){
        focusedElement = wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    //Click on the focused element
    public static void click(){
        focusedElement.click();
    }

    //SendKeys to the focused element
    public static void compose(String string){
        focusedElement.sendKeys(string);
    }

    //Check to see if the focused element is displayed
    public static void isDisplayed(){
        focusedElement.isDisplayed();
    }

    //Close and exit chromeDriver after execution
    public static void quit(){
        driver.quit();
    }
}
